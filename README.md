# This is going to be AWESOME

## Dependencies

Install the latest version of neovim (may not be available as a package).

> Needs atleast neovim version 0.7.0

Install `Packer` as per the instructions given on their 
[README.md](https://github.com/wbthomason/packer.nvim)

## Installation

(Re)move old configs from `~/.config` and clone this repository.

Then, from `nvim`, run,

```
:PackerCompile<cr>
:PackerInstall<cr>
```
