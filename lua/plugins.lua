local use = require('packer').use
require('packer').startup(
  function()
    use 'wbthomason/packer.nvim'
    use 'neovim/nvim-lspconfig'
    use 'hrsh7th/cmp-nvim-lsp'
    use 'hrsh7th/nvim-cmp'
    use 'hrsh7th/cmp-cmdline'
    use 'hrsh7th/cmp-path'
    use {
      "williamboman/mason.nvim",
      run = ":MasonUpdate" -- :MasonUpdate updates registry contents
    }
    use 'williamboman/mason-lspconfig.nvim'
    use 'nvim-treesitter/nvim-treesitter'
    use 'nvim-treesitter/nvim-treesitter-refactor'
    use {
      'nvim-lualine/lualine.nvim',
      requires = { 'kyazdani42/nvim-web-devicons', opt = true }
    }
    use 'APZelos/blamer.nvim'
    use 'folke/tokyonight.nvim'
    use 'sainnhe/edge'
    use 'jacoborus/tender.vim'
    use 'airblade/vim-gitgutter'
    use 'chrisbra/colorizer'
    use 'rcarriga/nvim-notify'
    use 'kyazdani42/nvim-tree.lua'
    use 'ryanoasis/vim-devicons'
    use 'junegunn/vim-easy-align'
    use 'famiu/bufdelete.nvim'
    use {
      'nvim-telescope/telescope.nvim', tag = '0.1.1',
      requires = { { 'nvim-lua/plenary.nvim' } }
    }
    use {
      'rmagatti/auto-session',
      config = function()
        require('auto-session').setup {
          log_level = 'info',
          auto_session_suppress_dirs = { '~/', '~/Projects' }
        }
      end
    }
    use {
      'weilbith/nvim-code-action-menu',
      cmd = 'CodeActionMenu',
    }
  end
)

require('lspConf')
require('lspKeymap')
require('gitgutterConf')
require('nvimtree')

require 'nvim-treesitter.configs'.setup {
  refactor = {
    highlight_definitions = { enable = true },
  },
}

vim.notify = require('notify')

require('mason').setup {
}

require('mason-lspconfig').setup {
  ensure_installed = { "lua_ls", "clangd", "pyright" },
}


require('lualine').setup {
  sections = {
    lualine_a = { 'mode' },
    lualine_b = {},
    lualine_c = { 'filename' },
    lualine_x = { 'encoding', 'fileformat', 'filetype' },
    lualine_y = { 'progress' },
    lualine_z = { 'location' }
  },

}

require('notify').setup { render = "compact", }
