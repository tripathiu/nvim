require('nvim-tree').setup {
  view = {
    adaptive_size = true,
    preserve_window_proportions = true,
  },
  renderer = {
    icons = {
      glyphs = {
        git = {
          unstaged = "M",
          staged = "S",
          unmerged = "C",
          renamed = "R",
          untracked = "U",
          deleted = "D",
          ignored = "i"
        }
      },
      git_placement = 'after'
    }
  }
}


--
