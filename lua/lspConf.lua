local lspConfig = require('lspconfig')
local capabilities = vim.lsp.protocol.make_client_capabilities()

capabilities = require('cmp_nvim_lsp').default_capabilities(capabilities)

-- Disable signs
vim.lsp.handlers["textDocument/publishDiagnostics"] = vim.lsp.with(
  vim.lsp.diagnostic.on_publish_diagnostics, {
  signs = false,
  virtual_text = false,
  underline = true,
}
)

-- on attach local function for keymaps, etc., see file --
local on_attach = function(client)
  vim.api.nvim_exec_autocmds('User', { pattern = 'LspAttached' })
end

--------------
-- jedi-lsp --
--------------
lspConfig.pyright.setup {
  capabilities = capabilities,
  on_attach = on_attach,
}
--
-------------
--  cmake  --
-------------
lspConfig.cmake.setup {
  capabilities = capabilities,
  on_attach = on_attach,
}
--
--------------
--  dartls  --
--------------
lspConfig.dartls.setup {
  capabilities = capabilities,
  on_attach = on_attach,
}

-------------
--   lua   --
------------
lspConfig.lua_ls.setup {
  settings = {
    Lua = {
      runtime = { version = 'LuaJIT' },
      diagnostics = { globals = { "vim" }, },
      workspace = { library = vim.api.nvim_get_runtime_file("", true), },
      telemetry = { enable = false },
    },
  }
}

--------------
--  clangd  --
--------------
lspConfig.clangd.setup {
  capabilities = capabilities,
  on_attach = on_attach,
  cmd = {
    "clangd",
    "--background-index",
    "--query-driver='/home/triutkarsh/gcc-arm-11.2-2022.02-x86_64-arm-none-eabi/bin/*gcc,/home/triutkarsh/gcc-arm-11.2-2022.02-x86_64-arm-none-eabi/bin/*g++",
  },
  filetypes = { "c", "cpp", "objc", "objcpp" },
}


--------------
--  clangd  --
--------------
lspConfig.yamlls.setup {
  capabilities = capabilities,
  on_attach = on_attach,
}
