if vim.g.vscode then
  return
end

require('plugins')

-- getters and setters --
-------------------------
--     lua            command      global_value       local_value ~
-- vim.o           :set                set                set
-- vim.bo/vim.wo   :setlocal            -                 set
-- vim.go          :setglobal          set                 -
--
vim.o.cmdheight = 2
vim.o.encoding = 'utf-8'
vim.o.updatetime = 300
vim.o.shiftwidth = 2
vim.o.tabstop = 2
vim.o.expandtab = true
vim.o.smartindent = true
vim.o.mouse = 'a'
vim.o.nu = true
vim.o.rnu = true
vim.o.nuw = 4 -- number width
vim.o.cursorline = true
vim.o.cursorlineopt = 'line'
vim.o.scl = 'yes:1'     --signs column
vim.o.scrolloff = 5     --keep five lines in context always
vim.o.wrap = false
vim.o.sidescrolloff = 0 --keep five lines in context always
vim.o.smartcase = true
vim.o.autoread = true
vim.o.splitright = true
vim.o.splitbelow = true
vim.o.timeoutlen = 500
vim.o.modeline = false -- see :h modeline, its kind of a dumb thing
vim.o.foldmethod = 'expr'
vim.o.foldexpr = 'nvim_treesitter#foldexpr()'
vim.o.foldenable = false
if not vim.fn.has('macunix') then
  vim.cmd [[
  let g:clipboard = {
        \   'name': 'win32yank-wsl',
        \   'copy': {
        \      '+': 'win32yank.exe -i --crlf',
        \      '*': 'win32yank.exe -i --crlf',
        \    },
        \   'paste': {
        \      '+': 'win32yank.exe -o --lf',
        \      '*': 'win32yank.exe -o --lf',
        \   },
        \   'cache_enabled': 0,
        \ }
  ]]
end

vim.cmd('au CursorHold,CursorHoldI,FocusGained,BufEnter * :checktime')
-- vim.cmd('autocmd BufEnter,TermOpen term://* startinsert') -- start insert as soon as inside a terminal
-- lua-vim variables --
-----------------------
-- vim.g.foo = 5     -- Set the g:foo Vimscript variable.
-- print(vim.g.foo)  -- Get and print the g:foo Vimscript variable.
-- vim.g.foo = nil   -- Delete (:unlet) the Vimscript variable.
-- vim.b[2].foo = 6  -- Set b:foo for buffer 2
vim.g.mapleader = " "

-- keymap set module --
-----------------------
-- Can add mapping to Lua functions
-- vim.keymap.set('n', 'lhs', function() print("real lua function") end)
--
-- Can use it to map multiple modes
-- vim.keymap.set({'n', 'v'}, '<leader>lr', vim.lsp.buf.references, { buffer=true })
--
-- Can add mapping for specific buffer
-- vim.keymap.set('n', '<leader>w', "<cmd>w<cr>", { silent = true, buffer = 5 })
--
-- Expr mappings
-- vim.keymap.set('i', '<Tab>', function()
--   return vim.fn.pumvisible() == 1 and "<C-n>" or "<Tab>"
-- end, { expr = true })
--
-- <Plug> mappings
-- vim.keymap.set('n', '[%', '<Plug>(MatchitNormalMultiBackward)')

local noremapsilent = { remap = false, silent = true }
local noremap = { remap = false }

-- these don't work on linux mint with alacritty
-- vim.keymap.set({ 'i', 'n', 'v', 's', 'o', 't' }, '<M-j>', '<C-n>', { noremap = true, expr = false})
-- vim.keymap.set({ 'i', 'n', 'v', 's', 'o', 't' }, '<M-k>', '<C-p>', { noremap = true, expr = false})
-- vim.keymap.set({ 'i', 'n', 'v', 's', 'o', 't', 'c' }, '<M-h>', '<C-p>', { noremap = true, expr = true})
-- vim.keymap.set({ 'i', 'n', 'v', 's', 'o', 't', 'c' }, '<M-l>', '<C-n>', { noremap = true, expr = true})
-- vim.keymap.set({ 'c' }, '<M-j>', '<C-n>', { noremap = true, expr = true})
-- vim.keymap.set({ 'c' }, '<M-k>', '<C-p>', { noremap = true, expr = true})

-- resizing
vim.keymap.set('n', '<C-Up>', ':resize +5<cr>', noremap)
vim.keymap.set('n', '<C-Down>', ':resize -5<cr>', noremap)
vim.keymap.set('n', '<C-Left>', ':vertical resize -5<cr>', noremap)
vim.keymap.set('n', '<C-Right>', ':vertical resize +5<cr>', noremap)

-- plain stuff
vim.keymap.set('i', 'jj', '<Esc>', noremapsilent)
vim.keymap.set('i', 'jJ', '<Esc>', noremapsilent)
vim.keymap.set('n', '<C-f><C-j>', ':nohlsearch<cr>', noremapsilent)
vim.keymap.set('n', '<leader>j', ':nohlsearch<cr>', noremapsilent)
vim.keymap.set({ 'i', 't' }, '<M-H>', '<BS>', noremapsilent)
vim.keymap.set({ 'i', 't' }, '<M-L>', '<DEL>', noremapsilent)
vim.keymap.set({ 'v' }, '<', '<gv', noremapsilent)
vim.keymap.set({ 'v' }, '>', '>gv', noremapsilent)

-- terminal bindings
vim.keymap.set('t', '<C-f><C-j>', '<C-\\><C-n>', noremapsilent)    -- exit terminal mode
vim.keymap.set({ 'c', 'v' }, '<C-f><C-j>', '<C-c>', noremapsilent) -- exit terminal mode
vim.keymap.set('i', '<C-f><C-j>', '<Esc>', noremapsilent)          -- exit terminal mode
-- vim.keymap.set('t', '<C-w>', '<C-\\><C-n><C-w>') -- exit terminal mode
vim.keymap.set('n', '<leader>/', ':vsp<cr>:term<cr>', noremapsilent)
vim.keymap.set('n', '<leader>?', ':sp<cr>:term<cr>', noremapsilent)
vim.keymap.set('n', '<C-h>', '<C-w>h', noremapsilent)
vim.keymap.set('n', '<C-j>', '<C-w>j', noremapsilent)
vim.keymap.set('n', '<C-k>', '<C-w>k', noremapsilent)
vim.keymap.set('n', '<C-l>', '<C-w>l', noremapsilent)
-- vim.keymap.set('n', '<Tab>', ':tabnext<cr>', {noremap=true})
vim.keymap.set('n', '<S-Tab>', ':tabprevious<cr>', { noremap = true }, noremapsilent)

-- edit and source nvim config
vim.keymap.set('n', '<leader>s', ':source ~/.config/nvim/init.lua<cr>', { remap = false })
vim.keymap.set('n', '<leader>S', ':vsp<cr>:e ~/.config/nvim/init.lua<cr>', { remap = false })

-- Plugins bindings
local ts = require('telescope.builtin')
vim.keymap.set({ 'n' }, '<leader>p', ts.find_files, { remap = false })
vim.keymap.set({ 'n' }, '<leader>gp', ':Telescope git_files<cr>', { remap = false })
vim.keymap.set({ 'n' }, '<leader>gb', ':Telescope git_branches<cr>', { remap = false })
vim.keymap.set({ 'n' }, '<leader>F', ts.live_grep, { remap = false })
vim.keymap.set({ 'n' }, '<leader>:', ':Telescope commands<cr>', { remap = false })
vim.keymap.set({ 'n' }, '<leader><Tab>', ts.buffers, { remap = false })
vim.keymap.set({ 'n' }, '<leader><C-t>', ':Telescope colorscheme<cr>', { remap = false })
vim.keymap.set({ 'n' }, '<leader>z', ':Telescope keymaps<cr>', { remap = false })
vim.keymap.set({ 'n' }, '<leader>Z', ts.help_tags, { remap = false })
vim.keymap.set({ 'n' }, '<leader><cr>', ts.resume, { remap = false })

-- nvimtree
vim.keymap.set({ 'n' }, '<leader>e', ':NvimTreeToggle<cr>', { remap = false })

-- new tab
vim.keymap.set({ 'n' }, '<leader>t', ':tabnew<cr>', { remap = false })
vim.keymap.set({ 'n' }, '<leader>w', ':tabclose<cr>', { remap = false })

-- diagnostics
vim.keymap.set({ 'n' }, 'r', vim.diagnostic.open_float, noremapsilent)

-- BufferNavigation
-- vim.keymap.set({'n'}, '<Tab>', ':bnext<cr>')
-- vim.keymap.set({'n'}, '<S-Tab>', ':bprevious<cr>')
-- vim.keymap.set({'n'}, '<C-Del>', ':bdelete<cr>')

-- numpad emulation
-- vim.keymap.set({'n', 'v', 'i'}, '<M-o>', '9')
-- vim.keymap.set({'n', 'v', 'i'}, '<M-i>', '8')
-- vim.keymap.set({'n', 'v', 'i'}, '<M-u>', '7')
-- vim.keymap.set({'n', 'v', 'i'}, '<M-k>', '5')
-- vim.keymap.set({'n', 'v', 'i'}, '<M-l>', '6')
-- vim.keymap.set({'n', 'v', 'i'}, '<M-j>', '4')
-- vim.keymap.set({'n', 'v', 'i'}, '<M-.>', '3')
-- vim.keymap.set({'n', 'v', 'i'}, '<M-,>', '2')
-- vim.keymap.set({'n', 'v', 'i'}, '<M-m>', '1')
-- vim.keymap.set({'n', 'v', 'i'}, '<M-/>', '0')


vim.keymap.set({ 'x' }, 'ga', '<Plug>(EasyAlign)', { noremap = true })
vim.keymap.set({ 'n' }, '<leader>f', vim.lsp.buf.format, { noremap = true })
vim.keymap.set({ 'n' }, '<leader>r', ':CodeActionMenu<cr>', { noremap = true })
vim.keymap.set({ 'v' }, '<leader>f', '<esc>:lua vim.lsp.buf.range_formatting()<cr>gv', { noremap = true })

-- extras
local api = vim.api
-- local culWhenWinEnter = api.nvim_create_augroup('CulWhenWinEnter', { clear = true })
-- api.nvim_create_autocmd('WinEnter', {
--   command = 'set cul',
--   group = culWhenWinEnter,
-- })
-- api.nvim_create_autocmd('WinLeave', {
--   command = 'set nocul',
--   group = culWhenWinEnter,
-- })

-- local statusLineColour = api.nvim_create_augroup('StatusLineColour', { clear = true })
-- api.nvim_create_autocmd({ 'InsertEnter' }, {
--   command = 'hi! StatusLine guibg=#a01930',
--   group = statusLineColour
-- })
-- api.nvim_create_autocmd({ 'TermEnter' }, {
--   command = 'hi! StatusLine guibg=#55913f',
--   group = statusLineColour
-- })
-- api.nvim_create_autocmd({ 'InsertLeave', 'TermLeave' }, {
--   command = 'hi! StatusLine guibg=#48517d',
--   group = statusLineColour
-- })

local englishGrp = api.nvim_create_augroup('English', { clear = true })
vim.api.nvim_create_autocmd({ "BufEnter", "BufWinEnter" }, {
  pattern = { "*.md" },
  command = "set spell",
  group = englishGrp
})
vim.api.nvim_create_autocmd({ "BufLeave", "BufWinLeave" }, {
  pattern = { "*.md" },
  command = "set nospell",
  group = englishGrp
})

-- auto format on save
local autoFmtGroup = api.nvim_create_augroup('AutoFmtGrp', { clear = true })
vim.api.nvim_create_autocmd({ "BufWritePre" }, {
  pattern = { "*.dart,*.cpp,*.c,*.hpp,*.h,*.hh,*.cc", },
  command = "lua vim.lsp.buf.format()",
  group = englishGrp
})


-- colorscheme
vim.o.termguicolors = true --false
vim.cmd('colo tokyonight-night')

-- dim inactive windows
vim.cmd [[
hi InctiveWindow guibg=#17252c
hi ActiveWindow guibg=#000000

" Call method on window enter
augroup WindowManagement
  autocmd!
  autocmd WinEnter * call Handle_Win_Enter()
augroup END

" Change highlight group of active/inactive windows
function! Handle_Win_Enter()
  setlocal winhighlight=Normal:ActiveWindow,NormalNC:InactiveWindow
endfunction
]]

